package pl.edu.pk.fmi.dojaniec.heavyqueuer;

import java.text.DateFormat;
import java.util.Calendar;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;

class WorkerProducer extends Worker {

    private long _sentCounter;
    private long _lastMessageTime;
    private long _lastSendBlockTime;
    private String _producerCreationDateTime;
    private long _lastStatsSentCounter;
    private long _lastStatsShowStatsTime;

    public WorkerProducer(Configuration config) {
	super(config, "ActiveMQ Producer");

	DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT,
		DateFormat.LONG);
	_producerCreationDateTime = formatter.format(Calendar.getInstance()
		.getTime());
    }

    @Override
    protected String showSelfConfiguration() {
	return "And I will produce stream of messages... With "
		+ _config.prodWaitTime() / 1000.0 + " seconds sleep time...";
    }

    @Override
    protected void clearJobStatistics() {
	_sentCounter = _lastStatsSentCounter = 0;
	_lastSendBlockTime = 0;
	_lastMessageTime = System.currentTimeMillis();
	_lastStatsShowStatsTime = _lastMessageTime + 1;
    }

    @Override
    protected String getJobStatistics() {
	long currentSent = _sentCounter;
	long messagesDelta = currentSent - _lastStatsSentCounter;

	long currentLastMessageTime = _lastMessageTime;
	long currentLastSendBlockTime = _lastSendBlockTime;

	String message = "";
	if (_lastStatsShowStatsTime < currentLastMessageTime) {
	    message = "Total messages sent: " + currentSent + " (+"
		    + messagesDelta + "). Last SendBlock time was: "
		    + currentLastSendBlockTime + "ms. ";
	}

	double nextSent = (getNextMessageTime() - System.currentTimeMillis()) / 1000.0;
	message += "And next message will be in: " + nextSent + " seconds...";

	_lastStatsShowStatsTime = System.currentTimeMillis();
	_lastStatsSentCounter = currentSent;

	return message;
    }

    @Override
    protected void executeJob() throws JMSException {
	MessageProducer mqProducer = getProducer();

	do {
	    handleMessage(mqProducer);
	} while (!shouldStop());

	mqProducer.close();
    }

    private MessageProducer getProducer() throws JMSException {
	return getSession().createProducer(getQueue());
    }

    private void handleMessage(MessageProducer mqProducer) throws JMSException {
	try {
	    Thread.sleep(_config.prodWaitTime());
	} catch (InterruptedException e) {
	    stop();
	    return;
	}

	TextMessage message = getNextMessage();
	sendMessage(mqProducer, message);

	if (_config.dumpWholeMessages())
	    dumpText("Message sent", message.getText());
    }

    private long getNextMessageTime() {
	return _lastMessageTime + _config.prodWaitTime();
    }

    private TextMessage getNextMessage() throws JMSException {
	String message = getWorkerIdentity();
	message += " from " + _producerCreationDateTime;
	message += "\n         | Message no. " + (_sentCounter + 1);

	return getSession().createTextMessage(message);
    }

    private void sendMessage(MessageProducer mqProducer, TextMessage message)
	    throws JMSException {

	_lastMessageTime = System.currentTimeMillis();
	mqProducer.send(message);

	_lastSendBlockTime = System.currentTimeMillis() - _lastMessageTime;
	++_sentCounter;
    }
}
