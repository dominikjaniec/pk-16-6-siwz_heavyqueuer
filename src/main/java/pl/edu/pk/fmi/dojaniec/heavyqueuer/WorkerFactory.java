package pl.edu.pk.fmi.dojaniec.heavyqueuer;

public class WorkerFactory {
	public static Worker CreateBy(Configuration config) {
		if (config.isProducer())
			return new WorkerProducer(config);

		return new WorkerConsumer(config);
	}
}
