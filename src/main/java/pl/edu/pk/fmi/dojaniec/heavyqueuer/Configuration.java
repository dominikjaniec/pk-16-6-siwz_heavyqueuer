package pl.edu.pk.fmi.dojaniec.heavyqueuer;

import org.apache.commons.cli.*;

class Configuration {

    private final CommandLine _commandLine;

    public Configuration(String[] args) throws ParseException {
	CommandLineParser parser = new BasicParser();
	_commandLine = parser.parse(DefinedOptions.get(), args);
    }

    public boolean isProducer() {
	return _commandLine.hasOption(DefinedOptions.IS_PRODUCER);
    }

    public long prodWaitTime() {
	if (!_commandLine.hasOption(DefinedOptions.PROD_WAITTIME))
	    return DefinedOptions.PROD_WAITTIME_DEFVAL;

	String value = _commandLine
		.getOptionValue(DefinedOptions.PROD_WAITTIME);
	return Long.parseLong(value);
    }

    public String mqHostUri() {
	return _commandLine.getOptionValue(DefinedOptions.MQ_HOST_URI);
    }

    public String mqUserName() {
	return _commandLine.getOptionValue(DefinedOptions.MQ_USERNAME);
    }

    public String mqUserPass() {
	return _commandLine.getOptionValue(DefinedOptions.MQ_USERPASS);
    }

    public String mqQueue() {
	return _commandLine.getOptionValue(DefinedOptions.MQ_QUEUE);
    }

    public boolean showStatistics() {
	return _commandLine.hasOption(DefinedOptions.SHOW_STATS);
    }

    public boolean dumpWholeMessages() {
	return _commandLine.hasOption(DefinedOptions.DUMP_MESSAGES);
    }

    public boolean logWithHeader() {
	return _commandLine.hasOption(DefinedOptions.LOG_WITH_HEADER);
    }

    public static void ShowHelp() {
	HelpFormatter formatter = new HelpFormatter();
	formatter.printHelp(getCommandlineSyntax(), DefinedOptions.get(), true);
    }

    private static String getCommandlineSyntax() {
	String classLocation = Configuration.class.getProtectionDomain()
		.getCodeSource().getLocation().getPath();
	String fileName = new java.io.File(classLocation).getName();

	return "java -jar " + fileName;
    }

    private static class DefinedOptions {
	public static final String IS_PRODUCER = "isProducer";
	public static final String PROD_WAITTIME = "prodWaitTime";
	public static final long PROD_WAITTIME_DEFVAL = 3500;
	public static final String MQ_HOST_URI = "mqHostUri";
	public static final String MQ_USERNAME = "mqUserName";
	public static final String MQ_USERPASS = "mqUserPass";
	public static final String MQ_QUEUE = "mqQueue";
	public static final String SHOW_STATS = "showStats";
	public static final String DUMP_MESSAGES = "dumpMessages";
	public static final String LOG_WITH_HEADER = "logWithHeader";

	public static Options get() {
	    Option isProducer = new Option(IS_PRODUCER, false,
		    "Instance will produce messages. Otherwise will be an consumer.");

	    Option prodWaitTime = new Option(PROD_WAITTIME, true,
		    "Producer: Define wait time between messages. Default will be set to: "
			    + PROD_WAITTIME_DEFVAL + "ms.");
	    prodWaitTime.setType(long.class);

	    Option mqHostUri = new Option(MQ_HOST_URI, true,
		    "Define ActiveMQ host URI.");
	    mqHostUri.setRequired(true);

	    Option mqUserName = new Option(MQ_USERNAME, true,
		    "Define ActiveMQ user name.");
	    mqUserName.setRequired(true);

	    Option mqUserPass = new Option(MQ_USERPASS, true,
		    "Define ActiveMQ user password.");
	    mqUserPass.setRequired(true);

	    Option mqQueue = new Option(MQ_QUEUE, true,
		    "Define ActiveMQ Queue.");
	    mqQueue.setRequired(true);

	    Option showStats = new Option(SHOW_STATS, false,
		    "Periodical messages statistics will be printed.");

	    Option dumpMessages = new Option(DUMP_MESSAGES, false,
		    "Every and whole messages will be printed.");

	    Option logWithHeader = new Option(LOG_WITH_HEADER, false,
		    "Alway log with worker's identity.");

	    return new Options().addOption(isProducer).addOption(prodWaitTime)
		    .addOption(mqHostUri).addOption(mqUserName)
		    .addOption(mqUserPass).addOption(mqQueue)
		    .addOption(showStats).addOption(dumpMessages)
		    .addOption(logWithHeader);
	}
    }
}
