package pl.edu.pk.fmi.dojaniec.heavyqueuer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;

class WorkerConsumer extends Worker {

    private long _receivedCounter;
    private long _lastMessageTime;
    private double _lastWaitTimespan;
    private long _lastStatsReceivedCounter;
    private long _lastStatsShowStatsTime;

    public WorkerConsumer(Configuration config) {
	super(config, "ActiveMQ Consumer");
    }

    @Override
    protected String showSelfConfiguration() {
	return "And I'm ready to listen...";
    }

    @Override
    protected void clearJobStatistics() {
	_receivedCounter = _lastStatsReceivedCounter = 0;
	_lastMessageTime = System.currentTimeMillis();
	_lastStatsShowStatsTime = _lastMessageTime + 1;
	_lastWaitTimespan = 0;
    }

    @Override
    protected String getJobStatistics() {
	long currentRecieved = _receivedCounter;
	long messagesDelta = currentRecieved - _lastStatsReceivedCounter;

	long currentLastMessageTime = _lastMessageTime;
	double currentLastWaiTimespan = _lastWaitTimespan;

	String message = "";
	if (_lastStatsShowStatsTime < currentLastMessageTime) {
	    message = "Total messages received: " + currentRecieved + " (+"
		    + messagesDelta + "). Last wait time was: "
		    + currentLastWaiTimespan + " seconds. ";
	}

	double currentWaitTimespan = (System.currentTimeMillis() - currentLastMessageTime) / 1000.0;
	message += "And now wait from " + currentWaitTimespan + " seconds...";

	_lastStatsShowStatsTime = System.currentTimeMillis();
	_lastStatsReceivedCounter = currentRecieved;

	return message;
    }

    @Override
    protected void executeJob() throws JMSException {
	MessageConsumer mqConsumer = getConsumer();

	do {
	    handleMessage(mqConsumer);
	} while (!shouldStop());

	mqConsumer.close();
    }

    private MessageConsumer getConsumer() throws JMSException {
	return getSession().createConsumer(getQueue());
    }

    private void handleMessage(MessageConsumer mqConsumer) throws JMSException {
	Message msq = mqConsumer.receive();
	updateJobReceivedStats();

	if (_config.dumpWholeMessages()) {
	    String message;
	    if (msq instanceof TextMessage) {
		TextMessage textMessage = (TextMessage) msq;
		message = textMessage.getText();
	    } else {
		message = msq.toString();
	    }

	    dumpText("Received", "[[" + message + "]]");
	}
    }

    private void updateJobReceivedStats() {
	long oldLastMessageTime = _lastMessageTime;
	_lastMessageTime = System.currentTimeMillis();
	_lastWaitTimespan = (_lastMessageTime - oldLastMessageTime) / 1000.0;

	++_receivedCounter;
    }
}
