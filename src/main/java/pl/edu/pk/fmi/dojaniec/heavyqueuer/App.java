package pl.edu.pk.fmi.dojaniec.heavyqueuer;

import javax.jms.JMSException;
import org.apache.commons.cli.ParseException;

public class App {

    public static void main(String[] args) throws JMSException {
	Configuration runConfig = null;
	try {
	    runConfig = new Configuration(args);
	} catch (ParseException e) {
	    Configuration.ShowHelp();
	    return;
	}

	WorkerFactory.CreateBy(runConfig).doJob();
    }
}
