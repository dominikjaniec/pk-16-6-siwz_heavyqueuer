package pl.edu.pk.fmi.dojaniec.heavyqueuer;

import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;

abstract class Worker {

    protected final long _statsPeriod = 7500;
    protected final Configuration _config;
    protected final String _workerName;
    protected final UUID _workerID;

    private Timer _statsTimer;
    private boolean _isRunning;
    private boolean _shouldJobStop;
    private Connection _mqConnection;
    private Session _mqSession;
    private Destination _mqQueue;

    protected Worker(Configuration config, String workerName) {
	_config = config;
	_workerName = workerName;
	_workerID = UUID.randomUUID();

	_isRunning = false;
	_shouldJobStop = false;
    }

    public void doJob() throws JMSException {
	showConfiguration();
	preareStatsTimer();

	if (!shouldStop()) {
	    prepareSession();

	    clearJobStatistics();
	    _isRunning = true;
	    executeJob();
	    _isRunning = false;
	}

	cleanUp();
	showSummary();
    }

    public void stop() {
	_shouldJobStop = true;
    }

    protected boolean shouldStop() {
	return _shouldJobStop;
    }

    protected Session getSession() {
	return _mqSession;
    }

    protected Destination getQueue() {
	return _mqQueue;
    }

    protected String getWorkerIdentity() {
	return "HeavyQueuer's \"" + _workerName + "\" (" + _workerID + ")";
    }

    protected void dumpText(String header, String text) {
	if (_config.logWithHeader()) {
	    text = getWorkerIdentity() + " | " + header + ":\n    " + text;
	}

	System.out.println(text);
    }

    protected abstract String showSelfConfiguration();

    protected abstract void clearJobStatistics();

    protected abstract String getJobStatistics();

    protected abstract void executeJob() throws JMSException;

    private void showConfiguration() {
	// TODO : Show base / common configuration...
	String message = "I'm a " + getWorkerIdentity() + "...\n";
	message += "    " + showSelfConfiguration() + "\n";

	System.out.println(message);
    }

    private void preareStatsTimer() {
	if (!_config.showStatistics())
	    return;

	TimerTask task = new TimerTask() {
	    @Override
	    public void run() {
		if (_isRunning)
		    dumpText("Statistics", getJobStatistics());
	    }
	};

	_statsTimer = new Timer("Stats timer", true);
	_statsTimer.schedule(task, _statsPeriod, _statsPeriod);
    }

    private void prepareSession() throws JMSException {
	ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
		_config.mqHostUri());

	_mqConnection = connectionFactory.createConnection(
		_config.mqUserName(), _config.mqUserPass());
	_mqConnection.start();

	_mqSession = _mqConnection.createSession(false,
		Session.AUTO_ACKNOWLEDGE);

	_mqQueue = _mqSession.createQueue(_config.mqQueue());
    }

    private void cleanUp() throws JMSException {
	if (_mqSession != null) {
	    _mqSession.close();
	    _mqSession = null;
	}

	if (_mqConnection != null) {
	    _mqConnection.close();
	    _mqConnection = null;
	}

	if (_statsTimer != null) {
	    _statsTimer.cancel();
	    _statsTimer = null;
	}
    }

    private void showSummary() {
	// TODO : Maybe show more statistics?
	dumpText("Summary", " -- done.");
    }
}
